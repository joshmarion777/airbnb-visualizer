/* eslint import/namespace: ['error', { allowComputed: true }] */
/* global window */
import React, {Component} from 'react';
import {connect} from 'react-redux';

import GenericInput from './input';
import Spinner from './spinner';
// import * as Demos from './demos';
import {updateParam} from '../actions/app-actions';
import DATA from '../data/hotels.json';
import { sample as ITEMS, type } from '../data/keys.js'
import '../stylesheet/_main.scss';
import '../stylesheet/_gallery.scss';

export default class InfoPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {hasFocus: false};
    this._blurTimer = null;
  }

  componentDidMount() {
    console.log("DATA -->", ITEMS);
  }
  

  _onFocus = () => {
    window.clearTimeout(this._blurTimer);
    this.setState({hasFocus: true});
  }

  _onBlur = () => {
    // New focus is not yet available when blur event fires.
    // Wait a bit and if no onfocus event is fired, remove focus
    this._blurTimer = window.setTimeout(() => {
      this.setState({hasFocus: false});
    }, 1);
  }

  render() {
    // const {demo, params, owner, meta} = this.props;
    const {hasFocus} = this.state;
    // const DemoComponent = Demos[demo];
    // const metaLoaded = owner === demo ? meta : {};

    return (
      <div
        className={`options-panel top-right ${hasFocus ? 'focus' : ''}`}
        tabIndex="0"
        onFocus={this._onFocus}
        onBlur={this._onBlur}
      >

        {Object.keys(ITEMS).length > 0 && <hr />}

        {Object.keys(ITEMS)
          .sort()
          .map((name, i) => (
            <GenericInput
              key={`${i}-${name}`}
              name={name}
              displayValue={ITEMS[name]}
              type={type[i]}
              // {...ITEMS[name]}
              // onChange={this.props.updateParam}
            />
          ))}

        {/* {this.props.children} */}

      </div>
    );
  }
}

// export default connect(
//   state => state.vis,
//   {updateParam}
// )(InfoPanel);
