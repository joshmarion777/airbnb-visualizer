This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Screenshots

## Singapore

### Zoomed out view:
![Singapore Airbnb Listing](screenshots/Singapore-zoomed-out-view.png)
    
### Zoomed in view of the listings:
![Singapore Airbnb Listing](screenshots/Singapore-zoomed-in-view.png)

### Further zoomed in:
![Singapore Airbnb Listing](screenshots/Singapore-further-zommed-in-view.png)

## New york:

### Zoomed out view:
![New york Listing](screenshots/New-york-zoomed-out-view.png)

### Zoomed in view:
![New york Listing](screenshots/New-york-zoomed-in-view.png)

### Further zoomed in:
![New york Listing](screenshots/New-york-further-zoomed-in-view.png)    
